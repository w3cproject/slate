# Introduction

Welcome to the Zuzan API!

You can use our API to access Zuzan API endpoints, which can get information about `services` in our database.

We have language bindings in Shell, PHP(soon) and JavaScript(soon)! You can view code examples in the dark area to the right, and you can switch the programming language of the examples with the tabs in the top right.
