# Authentication

> To authorize, use this code:

```php
require 'zuzanApi'

api = ZuzanApi::APIClient.authorize!(${TOKEN});
```

```shell
# Make sure to replace `test` with your login and password.
curl -X POST "https://zuzan.com/api/login_check"
  -H "Content-Type: application/json"
  -d '{"username":"test","password":"test"}'
```

> The above command returns JSON structured like this:

```json
{"token":"${TOKEN}"}
```

Zuzan uses `JSON Web Token Authentication` to allow access to the API.

Before send any request, you must be authorized with your personal `username` and `password`

Zuzan expects for the `token` to be included in all API requests to the server in a header that looks like the following:

`Authorization: Bearer ${TOKEN}`

<aside class="notice">
You must replace <code>${TOKEN}</code> with your `token`.
</aside>
