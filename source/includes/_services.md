# Services

## Get All

```php
require 'zuzanApi';

api = ZuzanApi::APIClient.authorize!(${TOKEN});
api.services.get;
```

```shell
curl -X GET "https://zuzan.com/api/v1/services?offset=1&limit=2" 
    -H "accept: application/json" 
    -H "Authorization: Bearer ${TOKEN}"
```

> The above command returns JSON structured like this:

```json
{
  "data": [
    {
        "id": "integer",
        "title": "string",
        "picture": [
          "string"
        ],
        "address": "string",
        "price": 0,
        "food": [
          "string"
        ],
        "mainPicture": "string",
        "city": "string",
        "phone": "string"
      },
    "..."
  ]
}
```

This endpoint retrieves all services.

### HTTP Request

`GET https://zuzan.com/api/v1/services`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
offset | 1 | You can specify the starting offset.
limit | 15 | You can limit the number of results returned.

<aside class="success">
Remember — a happy kitten is an authenticated kitten!
</aside>

## Get By ID

```php
require 'ZuzanApi'

api = ZuzanApi::APIClient.authorize!(${TOKEN})
api.serivces.get(2)
```

```shell
curl -X GET "https://zuzan.com/api/v1/services/1" 
    -H "accept: application/json" 
    -H "Authorization: Bearer ${TOKEN}"
```

```php
require 'zuzanApi';

api = ZuzanApi::APIClient.authorize!(${TOKEN});
api.services.get(2);
```

> The above command returns JSON structured like this:

```json
{
  "id": 1,
  "title": "string",
  "picture": [
    "string"
  ],
  "address": "string",
  "price": 0,
  "food": [
    "string"
  ],
  "mainPicture": "string",
  "city": "string",
  "phone": "string"
}
```

This endpoint retrieves a specific service by id.

### HTTP Request

`GET https://zuzan.com/api/v1/services/{id}`

### URL Parameters

Parameter | Description
--------- | -----------
id | The ID of the service to retrieve
